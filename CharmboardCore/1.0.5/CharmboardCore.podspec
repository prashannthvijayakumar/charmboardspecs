#
# Be sure to run `pod lib lint CharmboardCore.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CharmboardCore'
  s.version          = '1.0.5'
  s.summary          = 'Charmboard Core Essentials'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
All basic overlays and essential functions are entitled in Charmboard Core
                       DESC

  s.homepage         = 'https://bitbucket.org/prashannthvijayakumar/CharmboardCore'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'prashannth.vijayakumar' => 'prashannth.vijayakumar@charmboard.com' }
  s.source           = { :git => 'https://bitbucket.org/prashannthvijayakumar/CharmboardCore.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '4' }
  s.ios.deployment_target = '11.1'

  s.source_files = 'CharmboardCore/Classes/**/*.*'
  
  s.resource_bundles = {
     'CharmboardCore' => ['CharmboardCore/Assets/*.*']
  }

  s.public_header_files = 'CharmboardCore/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'PiwikTracker', '3.3.0'
  s.dependency 'JDFTooltips'
  s.dependency 'Haneke'
end
